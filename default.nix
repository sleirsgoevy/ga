{ pkgs ? import<nixpkgs>{},
  stdenv ? pkgs.stdenv,
  python3 ? pkgs.python3,
  makeWrapper ? pkgs.makeWrapper }:

stdenv.mkDerivation {
  name = "ga";
  buildInputs = [ python3 ];
  nativeBuild = [ makeWrapper ];
  phases = [ "installPhase" ];
  installPhase = ''
    mkdir -p $out/bin
    echo '#!${python3}/bin/python3' > $out/bin/ga
    cat ${./ga.py} >> $out/bin/ga
    chmod +x $out/bin/ga
  '';
}
