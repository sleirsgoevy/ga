import os.path, sys, ctypes, subprocess, errno, tempfile, shutil

libc = ctypes.CDLL('libc.so.6')
libc.unshare.restype = ctypes.c_int
libc.mount.restype = ctypes.c_int
libc.memfd_create.restype = ctypes.c_int

CLONE_NEWUSER = 1 << 28
CLONE_NEWNS = 1 << 17
MS_BIND = 1 << 12
MS_RDONLY = 1 << 0

def _raise_oserror():
    ctypes.pythonapi.PyErr_SetFromErrno(ctypes.py_object(OSError))

class UserError(Exception): pass
class GaFileError(SyntaxError):
    __module__ = 'builtins'

def system(*cmd, **kwds):
    if subprocess.call(cmd, **kwds):
        raise UserError("%r failed"%(cmd,))

def popen(*cmd, **kwds):
    p = subprocess.Popen(cmd, stdout=subprocess.PIPE, **kwds)
    ans = p.communicate()[0]
    if p.wait():
        raise UserError("%r faield"%(cmd,))
    return ans

def enter_ns():
    uid = os.getuid()
    gid = os.getgid()
    if libc.unshare(CLONE_NEWUSER | CLONE_NEWNS):
        _raise_oserror()
    with open('/proc/self/setgroups', 'w') as file: file.write('deny')
    with open('/proc/self/gid_map', 'w') as file: file.write('%d %d 1'%(gid, gid))
    with open('/proc/self/uid_map', 'w') as file: file.write('%d %d 1'%(uid, uid))
    os.setuid(uid)
    os.setgid(gid)

def mount(what, where, fs, options=None):
    flags = 0
    if options is not None:
        options = options.split(',')
        if 'bind' in options:
            flags |= MS_BIND
            options.remove('bind')
        if 'ro' in options:
            flags |= MS_RDONLY
            options.remove('ro')
        options = ','.join(options).encode()
        if not options: options = None
    if libc.mount(ctypes.c_char_p(what.encode()), ctypes.c_char_p(where.encode()), ctypes.c_char_p(fs if fs is None else fs.encode()), ctypes.c_int(flags), ctypes.c_char_p(options)):
        _raise_oserror()

def lazy_umount(path):
    if libc.umount(ctypes.c_char_p(path.encode()), MNT_DETACH):
        _raise_oserror()

def memfd_create(name, flags):
    ans = libc.memfd_create(ctypes.c_char_p(name.encode()), ctypes.c_int(flags))
    if ans < 0: _raise_oserror()
    return ans

TMPFS = '/sys/kernel/debug'
REPO = TMPFS+'/repo'
HOME = TMPFS+'/home'

def setup_mounts():
    mount('tmpfs', TMPFS, 'tmpfs')
    os.mkdir(REPO)
    os.mkdir(REPO+'/.git')
    os.mkdir(HOME)
    os.mkdir(TMPFS+'/lowerdir')
    os.mkdir(TMPFS+'/upperdir')
    os.mkdir(TMPFS+'/workdir')
    mount('.', TMPFS+'/lowerdir', None, 'bind')
    mount('overlay', REPO+'/.git', 'overlay', 'lowerdir='+TMPFS+'/lowerdir/.git,upperdir='+TMPFS+'/upperdir,workdir='+TMPFS+'/workdir')

def set_home(old, new):
    if old is not None:
        lazy_umount(HOME)
    if new is not None:
        mount(new, HOME, None, 'bind')

def splitfind(s, part, cnt=None):
    sp = s.split(None, cnt) if cnt is not None else s.split()
    i = 0
    for idx, q in enumerate(sp):
        i = s.find(q, i)
        if idx == part or q == part:
            return i
        i += len(q)
    return -1

def copy_with_command(command):
    if command is None: return shutil.copy2
    def copy(src, dst, *, follow_symlinks=True):
        if not follow_symlinks and os.path.islink(src):
            return shutil.copy2(src, dst)
        with open(src, 'rb') as f1:
            with open(dst, 'wb') as f2:
                system(command, stdin=f1, stdout=f2, shell=True)
        shutil.copymode(src, dst)
        shutil.copystat(src, dst)
    return copy

def process(repo, gafile, do_userns, do_preexec):
    on_branch = None
    home_dir = None
    def env():
        env = dict(os.environ)
        if home_dir is not None:
            env['HOME'] = HOME if do_userns else home_dir
        return env
    if do_userns:
        enter_ns()
    with open(gafile) as file:
        os.chdir(os.path.dirname(gafile) or '.')
        while os.getcwd() != '/' and not os.path.isdir('.git'):
            os.chdir('..')
        if not os.path.isdir('.git'):
            raise UserError("cannot find .git")
        lineno = 0
        for line in file:
            lineno += 1
            line = line.strip()
            if not line or line.startswith('#'):
                continue
            if line == 'COMMIT':
                cmd = 'COMMIT'
                args = None
            else:
                try: cmd, args = line.split(None, 1)
                except ValueError: raise GaFileError("unexpected format, expected CMD ARGS", (gafile, lineno, 0, line))
            if cmd == 'FROM':
                try: branch, = args.split()
                except ValueError: raise GaFileError("expected branch name", (gafile, lineno, splitfind(line, args, 1)+1 if args else 0, line))
                if on_branch is not None:
                    raise GaFileError("duplicate FROM directive", (gafile, lineno, 0, line))
                if do_userns:
                    setup_mounts()
                else:
                    os.makedirs(repo)
                    system('git', 'clone', '--shared', '.', repo)
                system('git', 'reset', '--hard', branch, cwd=repo, env=env())
                on_branch = branch
            elif on_branch is None:
                raise GaFileError("FROM must be the first directive", (gafile, lineno, 0, line))
            elif cmd == 'BRANCH':
                try: branch, = args.split()
                except ValueError: raise GaFileError("expected branch name", (gafile, lineno, splitfind(line, args, 1)+1 if args else 0, line))
                try: os.unlink(repo+'/.git/refs/heads/'+branch)
                except OSError as e:
                    if e.errno != errno.ENOENT: raise
                system('git', 'checkout', '-b', branch, cwd=repo, env=env())
                on_branch = branch
            elif cmd == 'HOME':
                new_home = os.path.abspath(os.path.expanduser(args.strip()))
                if not new_home: new_home = None
                if do_userns:
                    set_home(home_dir, new_home)
                home_dir = new_home
            elif cmd == 'ADD':
                if '|' in args:
                    args, command = args.split('|', 1)
                    command = command.strip()
                else:
                    command = None
                files = args.split()
                for idx, i in enumerate(files):
                    i = os.path.abspath(os.path.join('/', i))
                    if i == '/.git' or i.startswith('/.git/'):
                        raise GaFileError("ADD mentions .git", (gafile, lineno, splitfind(line, idx+1)+1, line))
                    if os.path.isdir(repo+i):
                        shutil.rmtree(repo+i)
                    else:
                        try: os.unlink(repo+i)
                        except OSError as e:
                            if e.errno != errno.ENOENT: raise
                    if os.path.isdir('.'+i):
                        shutil.copytree('.'+i, repo+i, copy_function=copy_with_command(command), symlinks=True, ignore_dangling_symlinks=True)
                    else:
                        copy_with_command(command)('.'+i, repo+i, follow_symlinks=False)
            elif cmd in ('CP', 'COPY'):
                if '|' in args:
                    args, command = args.split('|', 1)
                    command = command.strip()
                else:
                    command = None
                try: src, dst = args.split()
                except ValueError: raise SyntaxError(line)
                dst = os.path.abspath(os.path.join('/', dst))
                if dst == '/.git' or dst.startswith('/.git/'):
                    raise GaFileError("CP mentions .git", (gafile, lineno, splitfind(line, 2)+1, line))
                if os.path.isdir(repo+dst):
                    shutil.rmtree(repo+dst)
                else:
                    try: os.unlink(repo+dst)
                    except OSError as e:
                        if e.errno != errno.ENOENT: raise
                if os.path.isdir(src):
                    shutil.copytree(src, repo+dst, copy_function=copy_with_command(command), symlinks=True, ignore_dangling_symlinks=True)
                else:
                    copy_with_command(command)(src, repo+dst, follow_symlinks=False)
            elif cmd in ('PATCH', 'AM'):
                with open(memfd_create('patch.txt', 0), 'rb', closefd=True) as fd:
                    path = '/proc/self/fd/%d'%fd.fileno()
                    with open(path, 'wb') as wfile:
                        system(args, shell=True, stdout=wfile)
                    if cmd == 'PATCH':
                        with open(path, 'rb') as rfile:
                            system('patch', '-p1', cwd=repo, env=env(), stdin=rfile)
                    elif cmd == 'AM':
                        rev = popen('git', 'rev-parse', 'HEAD', cwd=repo, env=env()).decode('ascii')
                        with open(path, 'rb') as rfile:
                            system('git', 'am', cwd=repo, env=env(), stdin=rfile)
                        system('git', 'reset', '--soft', rev, cwd=repo, env=env())
            elif cmd == 'RUN':
                def preexec_fn():
                    try:
                        os.chdir(repo)
                        enter_ns()
                        mount('tmpfs', os.getenv('HOME'), 'tmpfs')
                        mount('tmpfs', '/home', 'tmpfs')
                    except:
                        sys.excepthook(*sys.exc_info())
                        raise
                system(args, shell=True, cwd=repo, env=env(), preexec_fn=preexec_fn if do_preexec else None)
            elif cmd == 'COMMIT':
                if args is None:
                    msg = ''
                    while not msg.rstrip().endswith('\nEND'):
                        msg += file.readline()
                        lineno += 1
                    msg = msg.rstrip()[:-4]
                else:
                    msg = args
                system('git', 'add', '-f', '.', cwd=repo, env=env())
                system('git', 'commit', '-m', msg, cwd=repo, env=env())
            else:
                raise GaFileError("unknown command", (gafile, lineno, 1, line))
    if on_branch is None:
        raise GaFileError("empty gafile", (gafile, 0, 0, ''))
    system('git', 'push', '--force', 'file:///'+os.getcwd(), on_branch, cwd=repo)

def usage():
    print('''\
usage: ga [-u] [-x] <gafile>

Options:
    -u      Use user namespaces to mount a tmpfs, instead of the default TEMPDIR
    -x      Conceal contents of $HOME from RUN commands\
''')
    exit(0)

def main(*args):
    tempfile.tempdir = os.environ.get('TEMPDIR', None)
    do_userns = False
    do_preexec = False
    pos_args = []
    for i in args:
        if i == '--help':
            usage()
        elif i.startswith('-') and i != '-':
            for j in i[1:]:
                if j == 'u':
                    do_userns = True
                elif j == 'x':
                    do_preexec = True
                elif j == 'h':
                    usage()
                else:
                    print('error: unsupported option -%c'%j, file=sys.stderr)
                    exit(1)
        else:
            pos_args.append(i)
    if not pos_args and os.path.exists('Gafile'):
        pos_args.append('Gafile')
    if len(pos_args) != 1:
        usage()
    syntaxerror = None
    try:
        if do_userns:
            process(REPO, pos_args[0], do_userns, do_preexec)
        else:
            with tempfile.TemporaryDirectory() as tmp:
                process(tmp+'/repo', pos_args[0], do_userns, do_preexec)
    except UserError as e:
        print('An error has occurred:', e, file=sys.stderr)
        exit(1)
    except GaFileError as e:
        syntaxerror = e
    if syntaxerror is not None:
        syntaxerror.__traceback__ = None
        sys.excepthook(GaFileError, syntaxerror, None)

if __name__ == '__main__':
    main(*sys.argv[1:])
